package mx.itesm.finalarquitecturasoftware.service;

import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.resources.DistancesSingleton;
import mx.itesm.finalarquitecturasoftware.resources.TownsSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;

@Service
public class ShortestRouteAlgorithmService {

    private static final Logger logger = LoggerFactory.getLogger(ShortestRouteAlgorithmService.class);

    public Town[] getShortestRoute(Town origin, Town destination) {

        // Start Greedy Algorithm
        LinkedList<Town> townsGraph = new LinkedList<>();

        // Add the origin
        townsGraph.add(origin);

        // Add all remaining cities to LinkedList
        LinkedList<Town> townList = TownsSingleton.getInstance().getTownsFilteredList(origin, destination);

        // Add nearest town to the last one in the graph
        while (townList.size() > 0) {
            Town nearestTown = townList.getFirst();
            double nearestDistance = DistancesSingleton.getInstance().getDistance(townsGraph.getLast().getId(), nearestTown.getId()).get();
            //System.out.printf("Last town: %d. Nearest Town: %d. Distance: %f\n", townsGraph.getLast().getId(), nearestTown.getId(), nearestDistance);

            for(Town currentTown : townList) {
                double currentDistance = DistancesSingleton.getInstance().getDistance(townsGraph.getLast().getId(), currentTown.getId()).get();
                if(currentDistance < nearestDistance) {
                    nearestTown = currentTown;
                    nearestDistance = currentDistance;
                }
            }

            townsGraph.add(nearestTown);
            townList.remove(nearestTown);
        }

        // Add the destination
        townsGraph.add(destination);

        Town[] result = townsGraph.toArray(new Town[townsGraph.size()]);
        // Finish Greedy Algorithm



        // Start 2-Opt swap
        boolean improved = true;

        while (improved) {
            double bestDistance = calculateRouteDistance(result);
            boolean shouldKeepLooking = true;
            for(int i = 1; i < result.length - 1 && shouldKeepLooking; i++) {
                for(int k = i+1; k < result.length - 1 && shouldKeepLooking; k++) {
                    Town[] newRoute = twoOptSwap(result, i, k);
                    double newDistance = calculateRouteDistance(newRoute);
                    if(newDistance < bestDistance) {
                        result = newRoute;
                        shouldKeepLooking = false;
                    }
                }
            }
            improved = !shouldKeepLooking;
        }

        return result;
    }

    public double calculateRouteDistance(Town[] towns) {
        double distance = 0.0;
        Town lastTown = towns[0];
        for(Town currentTown : towns) {
            distance += DistancesSingleton.getInstance().getDistance(lastTown.getId(), currentTown.getId()).get();
            lastTown = currentTown;
        }
        return distance;
    }

    public Town[] twoOptSwap(Town[] route, int i, int k) {
        LinkedList<Town> newRoute = new LinkedList<>();

        // 1. take route[0] to route[i-1] and add them in order to new_route
        for(int x = 0; x < i; x++) {
            newRoute.add(route[x]);
        }

        // 2. take route[i] to route[k] and add them in reverse order to new_route
        for(int x = k; x >= i; x--) {
            newRoute.add(route[x]);
        }

        // 3. take route[k+1] to end and add them in order to new_route
        for(int x = k+1; x < route.length; x++) {
            newRoute.add(route[x]);
        }

        return newRoute.toArray(new Town[newRoute.size()]);
    }
}
