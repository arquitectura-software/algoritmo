package mx.itesm.finalarquitecturasoftware.service;

import mx.itesm.finalarquitecturasoftware.domain.Distance;
import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.pojo.Message;
import mx.itesm.finalarquitecturasoftware.pojo.MessageResult;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.DistanceRequest;
import mx.itesm.finalarquitecturasoftware.pojo.customresponses.DistanceMatrixResult;
import mx.itesm.finalarquitecturasoftware.resources.DistancesSingleton;
import mx.itesm.finalarquitecturasoftware.resources.TownsSingleton;
import mx.itesm.finalarquitecturasoftware.validation.DistanceRequestValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Optional;

@Service
public class DistanceService {

    private static final Logger logger = LoggerFactory.getLogger(DistanceService.class);

    @Autowired
    private DistanceMatrixService distanceMatrixService;

    // Obtains the distance from one town to another
    public Result<Distance> getDistance(DistanceRequest distanceRequest) {
        Result<Distance> result = DistanceRequestValidation.validate(distanceRequest);

        if(result.getErrorCode() != null && result.getErrorCode() > 0) {
            return result;
        }

        Town startTown = TownsSingleton.getInstance().getTown(distanceRequest.getTown1().getId()).get();
        Town finishTown = TownsSingleton.getInstance().getTown(distanceRequest.getTown2().getId()).get();

        Distance distanceResult = new Distance();
        distanceResult.setOrigin(startTown);
        distanceResult.setDestination(finishTown);

        Optional<Double> distance = DistancesSingleton.getInstance().getDistance(startTown.getId(), finishTown.getId());
        if(!distance.isPresent()) {
            result.setErrorCode(500);
            result.setMessage(new Message("Error de servidor. "));
            return result;
        }

        distanceResult.setDistance(distance.get());

        result.setData(Optional.of(distanceResult));

        return result;
    }

    public MessageResult initDistances() {
        int success = 0;
        int failed = 0;

        double distances[][] = new double[111][111];

        for(int i = 0; i < distances.length; i++) {
            for(int j = 0; j < distances[i].length; j++) {
                distances[i][j] = -1.0;
            }
        }

        for(int i = 0; i < 111; i++) {
            Town origin = TownsSingleton.getInstance().getTown(i+1).get();

            LinkedList<Town> destinations = new LinkedList<>();
            Optional<DistanceMatrixResult> distanceMatrixResult;
            int j = 0;
            int counter = 0;

            // Get 0 - 19
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 20 - 39
            destinations = new LinkedList<>();
            j = 20;
            counter = 20;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 40 - 59
            destinations = new LinkedList<>();
            j = 40;
            counter = 40;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 60 - 79
            destinations = new LinkedList<>();
            j = 60;
            counter = 60;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 80 - 99
            destinations = new LinkedList<>();
            j = 80;
            counter = 80;
            while(counter < j + 20) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 20;
            }

            // Get 100 - 110
            destinations = new LinkedList<>();
            j = 100;
            counter = 100;
            while(counter < j + 11) {
                destinations.add(TownsSingleton.getInstance().getTown(counter+1).get());
                counter++;
            }
            distanceMatrixResult = distanceMatrixService.getDistancesByOrigin(origin, destinations.toArray(new Town[destinations.size()]));
            if(distanceMatrixResult.isPresent()) {
                for(int k = 0; k < distanceMatrixResult.get().rows[0].elements.length; k++) {
                    if(distanceMatrixResult.get().rows[0].elements[k].status.equals("OK")) {
                        distances[i][j + k] = distanceMatrixResult.get().rows[0].elements[k].distance.value;
                        success++;
                    } else {
                        distances[i][j + k] = -1;
                        failed++;
                    }
                }
            } else {
                failed += 11;
            }

            logger.info(String.format("Finish row: %d, total errors: %d", i, failed));
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < distances.length; i++) {
            for(int j = 0; j < distances[i].length; j++) {
                if(j==0) {
                    stringBuilder.append(String.format("%f", distances[i][j]));
                } else {
                    stringBuilder.append(String.format(",%f", distances[i][j]));
                }
            }
            if(i < distances.length - 1) {
                stringBuilder.append(",\n");
            }
        }
        System.out.println(stringBuilder.toString());

        MessageResult result = new MessageResult();
        if(failed > 0) {
            result.setErrorCode(500);
            result.setMessage(new Message(String.format("Failed %d: %s", failed, stringBuilder.toString())));
        } else {
            result.setMessage(new Message(String.format("Success %d: %s", success, stringBuilder.toString())));
        }
        return result;
    }
}
