package mx.itesm.finalarquitecturasoftware.service;

import com.google.gson.Gson;
import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.pojo.customresponses.DistanceMatrixResult;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.Reader;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


// Used to obtain the distance from one town to another using the Distance Matrix API from Google
@Service
public class DistanceMatrixService {

    private OkHttpClient client; // Used for http requests
    private String distanceMatrixApiUrl; // Distance Matrix API url
    private String apiKeys []; // Google API keys
    private int currentApiKey = 0; // Used to switch between keys when one has stopped working due to the free tier

    private static final Logger logger = LoggerFactory.getLogger(DistanceMatrixService.class);

    @PostConstruct
    private void initService() {
        client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        distanceMatrixApiUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";

        // New API keys can be added if needed
        apiKeys = new String[] {
                "AIzaSyAvj1kkx7ajqhYfd2oSy4wkTNQ47C5MPHA",
                "AIzaSyDHE3RD2fwu56cfB0utSFWF92g_RzqOjxY",
                "AIzaSyChC54EYNegNxJsiIGNoC8-RwYrIaqKxpM",
                "AIzaSyC39vbskLhHnGhNOWYmA_yy5dhjB25hszI",
                "AIzaSyC0MaQbGsB3abZDnSz8L3RKXUralzT_aPE",
                "AIzaSyBepmsqMx5GI27IDs3sCS_MokcA7W1Psgs",
                "AIzaSyAbwMvjyt2Iqby6UMqipcdcWvGNL_oYPvw"
        };
    }

    // Obtain the distance from an origin to many destinations in order
    // A maximum of 25 destination towns can be obtained
    // This method also retries with another api key if the current api key has reached its limit
    // If all the API keys have reached their limit then the method returns Optional.empty()
    public Optional<DistanceMatrixResult> getDistancesByOrigin(Town origin, Town[] destinations) {
        Optional<DistanceMatrixResult> result = Optional.empty();
        boolean obtainedData = false;
        for(int i = 0; i < apiKeys.length && !obtainedData; i++) {
            this.currentApiKey %= apiKeys.length;
            String currentKey = this.apiKeys[this.currentApiKey];
            result = getDistancesByOrigin(origin, destinations, currentKey);
            if(result.isPresent()) {
                obtainedData = true;
            } else {
                this.currentApiKey++;
            }
        }
        return result;
    }

    // Obtains the distances from an origin to many destinations with a given API key
    // A maximum of 25 destinations can be sent
    // This method should only be called by this.Optional<DistanceMatrixResult> getDistancesByOrigin(Town origin, Town[] destinations)
    private Optional<DistanceMatrixResult> getDistancesByOrigin(Town origin, Town[] destinations, String apiKey) {
        String url = this.distanceMatrixApiUrl;
        url += String.format("&origins=%f,%f&destinations=", origin.getLatitude(), origin.getLongitude());

        StringBuilder stringDestinationsBuilder = new StringBuilder();
        for(int i = 0; i < destinations.length; i++) {
            if(i==0) {
                stringDestinationsBuilder.append(String.format("%f,%f", destinations[i].getLatitude(), destinations[i].getLongitude()));
            } else {
                stringDestinationsBuilder.append(String.format("|%f,%f", destinations[i].getLatitude(), destinations[i].getLongitude()));
            }
        }

        url +=  String.format("%s&key=%s", stringDestinationsBuilder.toString(), apiKey);

        Response response;

        try {
            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
            response = client.newCall(request).execute();

            if(response.code() >= 200 && response.code() < 300) {
                Reader in = response.body().charStream();
                BufferedReader reader = new BufferedReader(in);

                DistanceMatrixResult results = new Gson().fromJson(reader, DistanceMatrixResult.class);
                reader.close();
                if(results == null) {
                    throw new Exception("Error parsing JSON: DistanceMatrixRequest");
                }
                if(!results.status.equals("OK") || results.rows.length < 1 || results.rows[0].elements.length != destinations.length) {
                    throw new Exception(results.error_message + ": " + results.status + ", url: " + url);
                }

                return Optional.of(results);
            } else {
                throw new Exception("Wrong status code: DistanceMatrixRequest");
            }
        } catch (Exception e) {
            logger.error(e.getMessage() + ": " + apiKey);
        }

        return Optional.empty();
    }
}
