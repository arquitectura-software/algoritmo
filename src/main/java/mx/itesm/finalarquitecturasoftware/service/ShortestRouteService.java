package mx.itesm.finalarquitecturasoftware.service;

import mx.itesm.finalarquitecturasoftware.domain.ShortestRoute;
import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.ShortestRouteRequest;
import mx.itesm.finalarquitecturasoftware.resources.TownsSingleton;
import mx.itesm.finalarquitecturasoftware.validation.ShortestRouteValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShortestRouteService {

    private static final Logger logger = LoggerFactory.getLogger(ShortestRouteService.class);

    @Autowired
    private DistanceMatrixService distanceMatrixService;

    @Autowired
    private ShortestRouteAlgorithmService shortestRouteAlgorithmService;

    // Obtains the shortest route from an origin town to a destination town passing
    // through every town in the process
    public Result<ShortestRoute> getShortestRoute(ShortestRouteRequest shortestRouteRequest) {
        Result<ShortestRoute> result = ShortestRouteValidation.validate(shortestRouteRequest);

        if(result.getErrorCode() != null && result.getErrorCode() > 0) {
            return result;
        }

        Town startTown = TownsSingleton.getInstance().getTown(shortestRouteRequest.getTown1().getId()).get();
        Town finishTown = TownsSingleton.getInstance().getTown(shortestRouteRequest.getTown2().getId()).get();

        ShortestRoute shortestRoute = new ShortestRoute();
        shortestRoute.setOrigin(startTown);
        shortestRoute.setDestination(finishTown);
        shortestRoute.setTowns(shortestRouteAlgorithmService.getShortestRoute(shortestRoute.getOrigin(), shortestRoute.getDestination()));

        // Get total distance
        shortestRoute.setDistance(shortestRouteAlgorithmService.calculateRouteDistance(shortestRoute.getTowns()));

        result.setData(Optional.of(shortestRoute));
        return result;
    }

}
