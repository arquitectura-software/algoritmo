package mx.itesm.finalarquitecturasoftware.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;
import java.util.Scanner;

// Used to save in an array the distances obtained from the distances_res.csv dile
public class DistancesSingleton {

    private static final Logger logger = LoggerFactory.getLogger(DistancesSingleton.class);

    private static DistancesSingleton instance;
    private double distances[][];

    private DistancesSingleton() {
        initDistances();
    }

    private void initDistances() {
        // Add distances
        this.distances = new double[111][111];

        Scanner input = null;
        try {
            input = new Scanner(new File("distances_res.csv"));
            input.useDelimiter(",");
            int currentIndex = 0;
            while (input.hasNext()) {
                this.distances[currentIndex / distances.length][currentIndex % distances.length] = Double.parseDouble(input.next());
                currentIndex++;
            }
            for(int i = 0; i < this.distances.length; i++) {
                for(int j = 0; j < this.distances[i].length; j++) {
                    if(this.distances[i][j] < 0) {
                        throw new Exception("The array is not complete");
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            this.distances = null;
        }

        if(input != null) {
            input.close();
        }
    }

    public static DistancesSingleton getInstance() {
        if(instance == null) {
            instance = new DistancesSingleton();
        }
        return instance;
    }

    // Obtain distance from town1 to town2 with their ids as params
    public Optional<Double> getDistance(int town1, int town2) {
        if(this.distances == null) {
            return Optional.empty();
        }
        return Optional.of(this.distances[town1-1][town2-1]);
    }
}
