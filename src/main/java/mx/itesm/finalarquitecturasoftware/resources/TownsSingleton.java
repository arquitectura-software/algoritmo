package mx.itesm.finalarquitecturasoftware.resources;

import mx.itesm.finalarquitecturasoftware.domain.Town;

import java.util.*;


// Used to store all the towns: their id, name, state, latitude and longitude
public class TownsSingleton {

    private static TownsSingleton instance;
    private HashMap<Integer, Town> townHashMap;

    private TownsSingleton() {
        townHashMap = new HashMap<>();

        // Add towns
        townHashMap.put(1, new Town(1, "Tecate", "Baja California", 32.5619348,-116.6577903));
        townHashMap.put(2, new Town(2, "Loreto", "Baja California Sur", 26.0151006,-111.3712463));
        townHashMap.put(3, new Town(3, "Todos Santos", "Baja California Sur", 23.4558908,-110.2528571));
        townHashMap.put(4, new Town(4, "Batopilas", "Chihuahua", 27.0305833,-107.7409999));
        townHashMap.put(5, new Town(5, "Casas Grandes", "Chihuahua", 30.3812962,-107.9794421));
        townHashMap.put(6, new Town(6, "Creel", "Chihuahua", 27.7500145,-107.6545769));
        townHashMap.put(7, new Town(7, "Cuatro Ciénegas", "Coahuila", 26.9839463,-102.0701078));
        townHashMap.put(8, new Town(8, "Arteaga", "Coahuila", 25.4431848,-100.8822049));
        townHashMap.put(9, new Town(9, "Viesca", "Coahuila", 25.3416307,-102.8321414));
        townHashMap.put(10, new Town(10, "Candela", "Coahuila", 26.8357869,-100.675851));
        townHashMap.put(11, new Town(11, "Guerrero", "Coahuila", 28.3171517,-100.3950888));
        townHashMap.put(12, new Town(12, "Parras", "Coahuila", 25.4508878,-102.2144019));
        townHashMap.put(13, new Town(13, "Mapimí", "Durango", 25.8343441,-103.8613969));
        townHashMap.put(14, new Town(14, "Linares", "Nuevo León", 24.8606078,-99.5920151));
        townHashMap.put(15, new Town(15, "Santiago", "Nuevo León", 25.372212,-100.5612955));
        townHashMap.put(16, new Town(16, "Cosalá", "Sinaloa", 24.4175993,-106.7089372));
        townHashMap.put(17, new Town(17, "El Rosario", "Sinaloa", 22.7499986,-105.3886206));
        townHashMap.put(18, new Town(18, "Mocorito", "Sinaloa", 25.4824706,-107.9307227));
        townHashMap.put(19, new Town(19, "El Fuerte", "Sinaloa", 26.4161063,-108.6354629));
        townHashMap.put(20, new Town(20, "Magdalena de Kino", "Sonora", 30.6315316,-111.0103211));
        townHashMap.put(21, new Town(21, "Álamos", "Sonora", 27.0272506,-108.9566111));
        townHashMap.put(22, new Town(22, "Mier", "Tamaulipas", 26.433117,-99.1709487));
        townHashMap.put(23, new Town(23, "Tula", "Tamaulipas", 22.994802,-99.7331275));
        townHashMap.put(24, new Town(24, "Calvillo", "Aguascalientes", 21.8494095,-102.7322151));
        townHashMap.put(25, new Town(25, "San José de Gracia", "Aguascalientes", 22.1426209,-102.6637533));
        townHashMap.put(26, new Town(26, "Real de Asientos", "Aguascalientes", 22.2393018,-102.0997842));
        townHashMap.put(27, new Town(27, "Comala", "Colima", 19.3195993,-103.7704053));
        townHashMap.put(28, new Town(28, "Valle de Bravo", "Estado de México", 19.1820952,-100.1634028));
        townHashMap.put(29, new Town(29, "Aculco", "Estado de México", 20.1356863,-99.9684916));
        townHashMap.put(30, new Town(30, "El Oro", "Estado de México", 19.799804,-100.1498739));
        townHashMap.put(31, new Town(31, "San Juan Teotihuacán y San Martín de las Pirámides", "Estado de México", 19.6982109,-98.8799681));
        townHashMap.put(32, new Town(32, "Villa del Carbón", "Estado de México", 19.7317654,-99.4837233));
        townHashMap.put(33, new Town(33, "Tepotzotlán", "Estado de México", 19.7192639,-99.2350336));
        townHashMap.put(34, new Town(34, "Ixtapan de la Sal", "Estado de México", 18.8435782,-99.6962907));
        townHashMap.put(35, new Town(35, "Metepec", "Estado de México", 19.2621401,-99.6339932));
        townHashMap.put(36, new Town(36, "Malinalco", "Estado de México", 19.3229774,-100.7263857));
        townHashMap.put(37, new Town(37, "Mineral de Pozos", "Guanajuato", 21.2221935,-100.5056355));
        townHashMap.put(38, new Town(38, "Jalpa de Cánovas", "Guanajuato", 20.8728954,-103.0742158));
        townHashMap.put(39, new Town(39, "Yuriria", "Guanajuato", 20.1663427,-101.3510827));
        townHashMap.put(40, new Town(40, "Salvatierra", "Guanajuato", 20.2143921,-100.9112738));
        townHashMap.put(41, new Town(41, "Dolores Hidalgo", "Guanajuato", 21.1591122,-100.9665343));
        townHashMap.put(42, new Town(42, "Taxco", "Guerrero", 18.5560121,-99.6264337));
        townHashMap.put(43, new Town(43, "Real del Monte", "Hidalgo", 20.1404204,-98.6953611));
        townHashMap.put(44, new Town(44, "Mineral del Chico", "Hidalgo", 20.2133761,-98.7349602));
        townHashMap.put(45, new Town(45, "Huichapan", "Hidalgo", 20.3735795,-99.6673465));
        townHashMap.put(46, new Town(46, "Tecozautla", "Hidalgo", 20.5327515,-99.6567803));
        townHashMap.put(47, new Town(47, "Huasca de Ocampo", "Hidalgo", 20.2038618,-98.5847428));
        townHashMap.put(48, new Town(48, "Tequila", "Jalisco", 20.8811927,-103.8440795));
        townHashMap.put(49, new Town(49, "San Sebastián del Oeste", "Jalisco", 20.7620075,-104.862902));
        townHashMap.put(50, new Town(50, "Talpa de Allende", "Jalisco", 20.3851006,-104.8382485));
        townHashMap.put(51, new Town(51, "Lagos de Moreno", "Jalisco", 21.3552068,-101.9580522));
        townHashMap.put(52, new Town(52, "Mazamitla", "Jalisco", 19.9192071,-103.0388436));
        townHashMap.put(53, new Town(53, "Mascota", "Jalisco", 20.5284992,-104.8099849));
        townHashMap.put(54, new Town(54, "Tapalpa", "Jalisco", 19.9432544,-103.7757101));
        townHashMap.put(55, new Town(55, "Pátzcuaro", "Michoacán", 19.9432544,-103.7757101));
        townHashMap.put(56, new Town(56, "Angangueo", "Michoacán", 19.6229248,-100.3071543));
        townHashMap.put(57, new Town(57, "Cuitzeo", "Michoacán", 19.9870353,-101.2548206));
        townHashMap.put(58, new Town(58, "Santa Clara del Cobre", "Michoacán", 19.3987649,-101.659167));
        townHashMap.put(59, new Town(59, "Tacámbaro", "Michoacán", 19.2500859,-101.6020561));
        townHashMap.put(60, new Town(60, "Jiquilpan", "Michoacán", 19.9917179,-102.7321221));
        townHashMap.put(61, new Town(61, "Tzintzuntzan", "Michoacán", 19.6292321,-101.5902741));
        townHashMap.put(62, new Town(62, "Tlalpujahua", "Michoacán", 19.8086999,-100.1841271));
        townHashMap.put(63, new Town(63, "Tlayacapan", "Morelos", 18.9576481,-98.9903093));
        townHashMap.put(64, new Town(64, "Tepoztlán", "Morelos", 18.987323,-99.1100623));
        townHashMap.put(65, new Town(65, "Jala", "Nayarit", 21.103929,-104.4488115));
        townHashMap.put(66, new Town(66, "Sayulita", "Nayarit", 20.8697747,-105.4475249));
        townHashMap.put(67, new Town(67, "Tequisquiapan", "Querétaro", 20.5344676,-99.9279639));
        townHashMap.put(68, new Town(68, "Cadereyta", "Querétaro", 20.6919488,-99.8388237));
        townHashMap.put(69, new Town(69, "Jalpan de Serra", "Querétaro", 21.2173121,-99.4784638));
        townHashMap.put(70, new Town(70, "San Joaquín", "Querétaro", 20.9145458,-99.5725651));
        townHashMap.put(71, new Town(71, "Bernal", "Querétaro", 20.7385123,-99.948107));
        townHashMap.put(72, new Town(72, "Xilitla", "San Luis Potosí", 21.3853729,-99.0010751));
        townHashMap.put(73, new Town(73, "Real de Catorce", "San Luis Potosí", 23.6914309,-100.8909359));
        townHashMap.put(74, new Town(74, "Tlaxco", "Tlaxcala", 19.6234285,-98.1364785));
        townHashMap.put(75, new Town(75, "Huamantla", "Tlaxcala", 19.3199755,-97.9631099));
        townHashMap.put(76, new Town(76, "Jerez", "Zacatecas", 22.6464226,-103.0205916));
        townHashMap.put(77, new Town(77, "Nochistlán", "Zacatecas", 21.3668507,-102.8595421));
        townHashMap.put(78, new Town(78, "Pinos", "Zacatecas", 22.2724079,-102.1146855));
        townHashMap.put(79, new Town(79, "Teúl", "Zacatecas", 21.4655724,-103.4690628));
        townHashMap.put(80, new Town(80, "Sombrerete", "Zacatecas", 23.6296816,-103.6542198));
        townHashMap.put(81, new Town(81, "Palizada", "Campeche", 18.2605185,-92.1025743));
        townHashMap.put(82, new Town(82, "San Cristóbal de las Casas", "Chiapas", 16.7312816,-92.6774192));
        townHashMap.put(83, new Town(83, "Chiapa de Corzo", "Chiapas", 16.6010774,-93.2340403));
        townHashMap.put(84, new Town(84, "Comitán", "Chiapas", 16.2327166,-92.1653785));
        townHashMap.put(85, new Town(85, "Palenque", "Chiapas", 17.5252764,-92.018578));
        townHashMap.put(86, new Town(86, "Capulálpam", "Oaxaca", 17.3060339,-96.453532));
        townHashMap.put(87, new Town(87, "Huautla de Jiménez", "Oaxaca", 18.1308299,-96.8611835));
        townHashMap.put(88, new Town(88, "San Pablo Villa de Mitla", "Oaxaca", 16.9204898,-96.4045912));
        townHashMap.put(89, new Town(89, "San Pedro y San Pablo Teposcolula", "Oaxaca", 17.4928807,-97.658411));
        townHashMap.put(90, new Town(90, "Mazunte", "Oaxaca", 15.6675026,-96.5581551));
        townHashMap.put(91, new Town(91, "Cuetzalan", "Puebla", 20.0197857,-97.5336042));
        townHashMap.put(92, new Town(92, "Atlixco", "Puebla", 18.9044198,-98.4816661));
        townHashMap.put(93, new Town(93, "Chignahuapan", "Puebla", 19.8377303,-98.0515937));
        townHashMap.put(94, new Town(94, "Huauchinango", "Puebla", 20.1733542,-98.0901372));
        townHashMap.put(95, new Town(95, "Pahuatlán", "Puebla", 20.2767439,-98.164526));
        townHashMap.put(96, new Town(96, "Tlatlauquitepec", "Puebla", 19.8388121,-97.6358926));
        townHashMap.put(97, new Town(97, "Xicotepec", "Puebla", 20.3315774,-98.0366805));
        townHashMap.put(98, new Town(98, "Zacatlán de las Manzanas", "Puebla", 19.9605231,-98.1556584));
        townHashMap.put(99, new Town(99, "Cholula", "Puebla", 19.0762898,-98.3045004));
        townHashMap.put(100, new Town(100, "Tapijulapa", "Tabasco", 17.4597672,-92.7861256));
        townHashMap.put(101, new Town(101, "Isla Mujeres", "Quintana Roo", 21.2320896,-86.7667381));
        townHashMap.put(102, new Town(102, "Tulum", "Quintana Roo", 20.2096191,-87.4893856));
        townHashMap.put(103, new Town(103, "Bacalar", "Quintana Roo", 18.6696524,-88.41682));
        townHashMap.put(104, new Town(104, "Orizaba", "Veracruz", 18.8590877,-97.1317859));
        townHashMap.put(105, new Town(105, "Xico", "Veracruz", 19.4218133,-97.0221365));
        townHashMap.put(106, new Town(106, "Coscomatepec", "Veracruz", 19.0730097,-97.0549689));
        townHashMap.put(107, new Town(107, "Papantla", "Veracruz", 20.4566697,-97.3331227));
        townHashMap.put(108, new Town(108, "Zozocolco", "Veracruz", 20.1404939,-97.5849467));
        townHashMap.put(109, new Town(109, "Coatepec", "Veracruz", 19.4761089,-96.9936181));
        townHashMap.put(110, new Town(110, "Izamal", "Yucatán", 20.9331328,-89.038009));
        townHashMap.put(111, new Town(111, "Valladolid", "Yucatán", 20.688114,-88.2204456));
    }

    public static TownsSingleton getInstance() {
        if(instance == null) {
            instance = new TownsSingleton();
        }
        return instance;
    }

    public LinkedList<Town> getTownsFilteredList(Town origin, Town destination) {
        LinkedList<Town> townFilteredList = new LinkedList<>();

        for (Town currentTown : townHashMap.values()){
            if(!(currentTown.getId().equals(origin.getId()) || currentTown.getId().equals(destination.getId()))) {
                townFilteredList.add(currentTown);
            }
        }
        return townFilteredList;
    }

    public Optional<Town> getTown(Integer id) {
        Town town = townHashMap.get(id);
        if(town == null) {
            return Optional.empty();
        }
        return Optional.of(town);
    }
}
