package mx.itesm.finalarquitecturasoftware.endpoint;

import mx.itesm.finalarquitecturasoftware.domain.ShortestRoute;
import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.pojo.ListResponse;
import mx.itesm.finalarquitecturasoftware.pojo.ListResult;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.ShortestRouteRequest;
import mx.itesm.finalarquitecturasoftware.service.ShortestRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class ShortestRouteEndpoint {

    @Autowired
    private ShortestRouteService shortestRouteService;

    @GET
    @Path("/shortest_route")
    public Response getShortestRoute(@QueryParam("town1") String town1, @QueryParam("town2") String town2){
        ShortestRouteRequest request = new ShortestRouteRequest();
        request.setTown1(new Town());
        request.setTown2(new Town());
        if(town1 != null) {
            try {
                Integer town1Id = Integer.parseInt(town1.trim());
                request.getTown1().setId(town1Id);
            } catch (Exception e) {
                request.getTown1().setId(0);
            }
        } else {
            request.getTown1().setId(null);
        }
        if(town2 != null) {
            try {
                Integer town2Id = Integer.parseInt(town2.trim());
                request.getTown2().setId(town2Id);
            } catch (Exception e) {
                request.getTown2().setId(0);
            }
        } else {
            request.getTown2().setId(null);
        }

        Result<ShortestRoute> result = shortestRouteService.getShortestRoute(request);
        Response response;
        if(result.getData().isPresent()) {
            response = Response.ok(result.getData().get()).build();
        } else {
            if(result.getErrorCode() != null && result.getErrorCode() > 0) {
                response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
            } else {
                response = Response.status(500).entity("Error de servidor. ").build();
            }
        }
        return response;
    }

}
