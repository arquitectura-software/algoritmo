package mx.itesm.finalarquitecturasoftware.endpoint;


import mx.itesm.finalarquitecturasoftware.domain.Distance;
import mx.itesm.finalarquitecturasoftware.domain.Town;
import mx.itesm.finalarquitecturasoftware.pojo.Message;
import mx.itesm.finalarquitecturasoftware.pojo.MessageResult;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.DistanceRequest;
import mx.itesm.finalarquitecturasoftware.service.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("")
@Produces(MediaType.APPLICATION_JSON)
public class DistancesEndpoint {

    @Autowired
    private DistanceService distanceService;

    // Used to obtain the csv file (printed in console) of all the distances from any given town to another
    // This should only be done once, then, the file should be stored at the root of the project as
    // distances_res.csv
    @POST
    @Path("/init")
    public Response init() {
        MessageResult result = distanceService.initDistances();
        Response response;
        if(result.getErrorCode() == null || result.getErrorCode() < 1) {
            response = Response.ok(result.getMessage()).build();
        } else if(result.getMessage() != null) {
            response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
        } else {
            response = Response.serverError().entity(new Message("Error de servidor.")).build();
        }
        return response;
    }

    // Obtains the distance from one town to another using their ids. The query params are the towns ids
    // that go from 1 to 111
    // This method uses the distances_res.csv file to obtain the distance from one to to another
    @GET
    @Path("/distance")
    public Response getDistance(@QueryParam("town1") String town1, @QueryParam("town2") String town2) {
        DistanceRequest request = new DistanceRequest();
        request.setTown1(new Town());
        request.setTown2(new Town());
        if(town1 != null) {
            try {
                Integer town1Id = Integer.parseInt(town1.trim());
                request.getTown1().setId(town1Id);
            } catch (Exception e) {
                request.getTown1().setId(0);
            }
        } else {
            request.getTown1().setId(null);
        }
        if(town2 != null) {
            try {
                Integer town2Id = Integer.parseInt(town2.trim());
                request.getTown2().setId(town2Id);
            } catch (Exception e) {
                request.getTown2().setId(0);
            }
        } else {
            request.getTown2().setId(null);
        }

        Result<Distance> result = distanceService.getDistance(request);
        Response response;
        if(result.getData().isPresent()) {
            response = Response.ok(result.getData().get()).build();
        } else {
            if(result.getErrorCode() != null && result.getErrorCode() > 0) {
                response = Response.status(result.getErrorCode()).entity(result.getMessage()).build();
            } else {
                response = Response.status(500).entity("Error de servidor. ").build();
            }
        }
        return response;
    }
}
