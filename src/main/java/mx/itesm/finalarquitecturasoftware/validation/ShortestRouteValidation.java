package mx.itesm.finalarquitecturasoftware.validation;

import mx.itesm.finalarquitecturasoftware.domain.ShortestRoute;
import mx.itesm.finalarquitecturasoftware.pojo.Message;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.ShortestRouteRequest;

// Validates a request to obtain the shortest route from one town to another passing through all the other
// 109 towns first
public class ShortestRouteValidation {

    public static Result<ShortestRoute> validate(ShortestRouteRequest shortestRouteRequest) {
        Result<ShortestRoute> result = new Result<>();
        result.setErrorCode(null);

        String message = "";

        if(shortestRouteRequest == null) {
            result.setErrorCode(400);
            message += "El cuerpo del post no puede ser nulo. ";
        } else {
            if(shortestRouteRequest.getTown1() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo inicial. ";
            } else {
                if(shortestRouteRequest.getTown1().getId() == null || shortestRouteRequest.getTown1().getId() < 1 || shortestRouteRequest.getTown1().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo inicial. ";
                }
            }

            if(shortestRouteRequest.getTown2() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo final. ";
            } else {
                if(shortestRouteRequest.getTown2().getId() == null || shortestRouteRequest.getTown2().getId() < 1 || shortestRouteRequest.getTown2().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo final. ";
                }
            }
        }

        result.setMessage(new Message(message));

        return result;
    }
}
