package mx.itesm.finalarquitecturasoftware.validation;

import mx.itesm.finalarquitecturasoftware.domain.Distance;
import mx.itesm.finalarquitecturasoftware.pojo.Message;
import mx.itesm.finalarquitecturasoftware.pojo.Result;
import mx.itesm.finalarquitecturasoftware.pojo.customrequests.DistanceRequest;

// Validates a request to obtain the distance from on town to another
public class DistanceRequestValidation {

    public static Result<Distance> validate(DistanceRequest distanceRequest) {
        Result<Distance> result = new Result<>();
        result.setErrorCode(null);

        String message = "";

        if(distanceRequest == null) {
            result.setErrorCode(400);
            message += "El cuerpo del post no puede ser nulo. ";
        } else {
            if(distanceRequest.getTown1() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo inicial. ";
            } else {
                if(distanceRequest.getTown1().getId() == null || distanceRequest.getTown1().getId() < 1 || distanceRequest.getTown1().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo inicial. ";
                }
            }

            if(distanceRequest.getTown2() == null) {
                result.setErrorCode(400);
                message += "Se tiene que enviar el pueblo final. ";
            } else {
                if(distanceRequest.getTown2().getId() == null || distanceRequest.getTown2().getId() < 1 || distanceRequest.getTown2().getId() > 111) {
                    result.setErrorCode(400);
                    message += "Id no válido para el pueblo final. ";
                }
            }
        }

        result.setMessage(new Message(message));

        return result;
    }

}
