package mx.itesm.finalarquitecturasoftware.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import mx.itesm.finalarquitecturasoftware.endpoint.DistancesEndpoint;
import mx.itesm.finalarquitecturasoftware.endpoint.ShortestRouteEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(JacksonJaxbJsonProvider.class);
        registerEndpoints();
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }

    public void registerEndpoints() {
        register(ShortestRouteEndpoint.class);
        register(DistancesEndpoint.class);
    }
}
