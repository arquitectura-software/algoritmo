package mx.itesm.finalarquitecturasoftware.pojo.customresponses;

public class DistanceMatrixElement {
    public DistanceMatrixElementObject distance;
    public DistanceMatrixElementObject duration;
    public String status;
}
