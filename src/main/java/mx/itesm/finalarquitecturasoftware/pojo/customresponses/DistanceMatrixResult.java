package mx.itesm.finalarquitecturasoftware.pojo.customresponses;

public class DistanceMatrixResult {
    public DistanceMatrixRow[] rows;
    public String status;
    public String error_message;
}
