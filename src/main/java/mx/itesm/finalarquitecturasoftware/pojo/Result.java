package mx.itesm.finalarquitecturasoftware.pojo;

import java.util.Optional;

public class Result<T> extends MessageResult {
    private Optional<T> data;

    public Result(){
        data = Optional.empty();
    }

    public Optional<T> getData() {
        return data;
    }

    public void setData(Optional<T> data) {
        this.data = data;
    }
}
