package mx.itesm.finalarquitecturasoftware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalArquitecturaSoftwareApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalArquitecturaSoftwareApplication.class, args);
	}
}
