package mx.itesm.finalarquitecturasoftware.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/*
* Used for the response of a distance
* */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Distance {

    private Town origin;
    private Town destination;

    private double distance;

    public Town getOrigin() {
        return origin;
    }

    public void setOrigin(Town origin) {
        this.origin = origin;
    }

    public Town getDestination() {
        return destination;
    }

    public void setDestination(Town destination) {
        this.destination = destination;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Distance{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", distance=" + distance +
                '}';
    }
}
