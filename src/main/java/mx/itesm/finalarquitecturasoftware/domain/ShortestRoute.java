package mx.itesm.finalarquitecturasoftware.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShortestRoute {

    private Town origin;
    private Town destination;
    private Double distance;
    private Double time;
    private Town[] towns;

    public Town getOrigin() {
        return origin;
    }

    public void setOrigin(Town origin) {
        this.origin = origin;
    }

    public Town getDestination() {
        return destination;
    }

    public void setDestination(Town destination) {
        this.destination = destination;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public Town[] getTowns() {
        return towns;
    }

    public void setTowns(Town[] towns) {
        this.towns = towns;
    }

    @Override
    public String toString() {
        return "ShortestRoute{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", distance=" + distance +
                ", time=" + time +
                ", towns=" + Arrays.toString(towns) +
                '}';
    }
}
